Simple Nintendo Nunchuck game controller

-Tested Game: http://freecode.com/projects/xgalagapp/

BOM:
- Arduino Nano
- Tacktile switch
- ADXL345 Module (https://www.tokopedia.com/ghostgrosir/adxl-345-3-axis-digital-accelerometer-module-adxl345)
- 433mhz RF Data UART module (http://www.cytron.com.my/p-rf-uart-433-1km),
you could replace this with another UART based RF module like bluetoooth etc
- Use emulation server: https://gitlab.com/crowja.root/SerialMouseEmulatorServer_Linux_X11